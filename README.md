Acción Directa
==============

![Acción Directa](img/accion-directa-captura.png)

Acción Directa es un medialab P2P (par a par) que permite crear proyectos en grupo sin la necesidad de usar servidores centralizados, en el podrás crear historietas, animaciones, video juegos, música y todos lo que imagines.
Basado en tecnogologías git y webdav nos permite tener versiones de proyectos e historial de cambios sin la necesidad de tener un servidor central.

Acción Directa is a medialab P2P (peer to peer) that allows to create projects in group without the need to use servers centralised, in the will be able to create cartoons, animations, video games, music and all what imagine.
Based in tecnogologías git and webdav allows us have versions of projects and record of changes without the need to have a central server.

BTC: 19qkh5dNVxL58o5hh6hLsK64PwEtEXVHXs  
LTC: LeFUtMaCRq1HYapFNPfCZoSiXn9A6KNDoK  
[JS-miner](https://tinyurl.com/lue7ezooyo3Es7TeHoaquae6Ruj6ph)
